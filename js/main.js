const handleStart = () => {
  console.log('handleStart');
};
const handleUpdate = () => {
  console.log('handleUpdate');
};
const handleComplete = () => {
  console.log('handleComplete');
};

const timeline = gsap.timeline({
  duration: 1,
  paused: true,
  onStart: handleStart,
  onUpdate: handleUpdate,
  onComplete: handleComplete,
});

timeline
  .from('body', {
    backgroundColor: '#fff',
    duration: 1.7,
    ease: 'none',
  })
  .fromTo(['h1', 'p'], {
    opacity: 0,
    y: -20,
  }, {
    opacity: 1,
    y: 0,
    duration: .6,
    ease: 'power1.out',
    stagger: .2,
  },
  '-=1')
  .from(['img', 'h2'], {
    opacity: 0,
    duration: .7,
    ease: 'none',
  })
  .fromTo('ul li', {
    opacity: 0,
    y: -20,
  }, {
    opacity: 1,
    y: 0,
    duration: .6,
    ease: 'power1.out',
    stagger: .2,
  });

const btnPlay = document.querySelector('#btnPlay');
const btnPause = document.querySelector('#btnPause');
const btnResume = document.querySelector('#btnResume');
const btnReverse = document.querySelector('#btnReverse');
const btnSpeedUp = document.querySelector('#btnSpeedUp');
const btnSlowDown = document.querySelector('#btnSlowDown');
const btnSeek = document.querySelector('#btnSeek');
const btnProgress = document.querySelector('#btnProgress');
const btnRestart = document.querySelector('#btnRestart');

btnPlay.addEventListener('click', () => timeline.play());
btnPause.addEventListener('click', () => timeline.pause());
btnResume.addEventListener('click', () => timeline.resume());
btnReverse.addEventListener('click', () => timeline.reverse());
btnSpeedUp.addEventListener('click', () => timeline.timeScale(2));
btnSlowDown.addEventListener('click', () => timeline.timeScale(.5));
btnSeek.addEventListener('click', () => timeline.seek(1));
btnProgress.addEventListener('click', () => timeline.progress(.5));
btnRestart.addEventListener('click', () => timeline.restart());

console.log('Timeline object', timeline);
console.log('Timeline duration', timeline.duration());
console.log('Timeline children', timeline.getChildren());